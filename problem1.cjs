const fs = require("fs");

function problem1(number, folderName) {
  new Promise((resolve, reject) => {
    createDirectory(resolve, reject, folderName);
  })
    .then((data) => {
      let fileWritePromises = [];
      console.log(data);

      let fileCreate = Array(number).fill(0).map((element, index) => {
          let writePromise = new Promise((resolve, reject) => {
            fs.writeFile(`${folderName}/file${index}.json`,JSON.stringify({ a: 1, b: 2, c: 3 }), "utf-8", (err) => {
                if (err) {
                  reject(`Error occured write file ${err}`);
                } else {
                  resolve(index);
                  console.log("File got created " + `${folderName}/file${index}.json`);
                }
            });
          })
          .then((index) => {
            let writePromise = new Promise((resolve, reject) => {
              fs.unlink(`${folderName}/file${index}.json`, (err) => {
                if (err) {
                  reject(`Error occured unlink file ${err}`);
                } else {
                  resolve();
                  console.log("File got deleted " + `${folderName}/file${index}.json`);
                }
              });
            });
          });
          fileWritePromises.push(writePromise);
        });
    })
    .catch((err)=> {
        console.error(err)
    })
}

function createDirectory(resolve, reject, folderName) {
  fs.mkdir(folderName, (err) => {
    if (err) {
      reject(`Error occured on directory: ${err}`);
    } else {
      resolve(`Create the node directory`);
    }
  });
}

module.exports = problem1;
