const path = require("path");
const problem2 = require("../problem2.cjs");

const fileName = path.resolve("../lipsum.txt");

problem2(fileName);
