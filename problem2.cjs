const fs = require("fs");
const path = require("path");

const newFile1 = path.resolve("upperCase.txt");
const newFile2 = path.resolve("lowerCase.txt");
const newFile3 = path.resolve("sorting.txt");
const newFile = path.resolve("../fileNames.txt");

const regex = "\n";

function problem2(fileName) {
    new Promise((resolve, reject) => {
        readFile(resolve, reject, fileName);
    })
    .then((data) => {
        let upperCaseConversion = data.toUpperCase();

        const promise1 = new Promise((resolve, reject) => {
            writeFile(resolve, reject, newFile1, upperCaseConversion);
        });

        const promise2 = new Promise((resolve, reject) => {
            writeFile(resolve, reject, newFile, newFile1 + regex);
        });

        return Promise.all([promise1, promise2]);
    })
    .then((data) => {
        return new Promise((resolve, reject) => {
            readFile(resolve, reject, newFile1);
        });
    })
    .then((data) => {
        let lowerCaseConversion = data.toLowerCase().split(". ");

        const promise3 = new Promise((resolve, reject) => {
            writeFile(resolve, reject, newFile2, lowerCaseConversion);
        });

        const promise4 = new Promise((resolve, reject) => {
            appendFile(resolve, reject, newFile, newFile2 + regex);
        });

        return Promise.all([promise3, promise4]);
    })
    .then((data) => {
        return new Promise((resolve, reject) => {
            readFile(resolve, reject, newFile2);
        });
    })
    .then((data) => {
        let sorting = data.split(" ").sort();

        const promise5 = new Promise((resolve, reject) => {
            writeFile(resolve, reject, newFile3, sorting);
        });

        const promise6 = new Promise((resolve, reject) => {
            appendFile(resolve, reject, newFile, newFile3 + regex);
        });

        return Promise.all([promise5, promise6]);
    })
    .then((data) => {
        return new Promise((resolve, reject) => {
            readFile(resolve, reject, newFile);
        });
    })
    .then((data) => {
        let arr = data.split(regex);

        let fileCreate = arr.map((element, index) => {
            if (arr[index] !== "") {
                fs.unlink(`${arr[index]}`, (err) => {
                    if (err) {
                        console.error(`Error occured on delete file 1: ${err}`);
                    } else {
                        console.log(`Deleted the ${arr[index]} file`);
                    }
                });
            }
        });
    })
    .catch((err)=>{
        console.error(err)
    })
}

function readFile(resolve, reject, fileName) {
    fs.readFile(fileName, "utf-8", (err, data) => {
    if (err) {
        reject(`Error occured on ${fileName} file: ${err}`);
    } else {
        resolve(data);
        console.log(`${fileName} file read done`);
    }
});
}

function writeFile(resolve, reject, fileName, data) {
    fs.writeFile(fileName, data, "utf-8", (err) => {
        if (err) {
            reject(`Error occured on ${fileName} file: ${err}`);
        } else {
            resolve(`${fileName} read done`);
        }
    });
}

function appendFile(resolve, reject, fileName, data) {
    fs.appendFile(fileName, data, "utf-8", (err) => {
    if (err) {
        reject(`Error occured on ${fileName} file: ${err}`);
    } else {
        resolve(`${fileName} join`);
    }
});
}

module.exports = problem2;
